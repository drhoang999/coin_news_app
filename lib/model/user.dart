class User {
  User({
    this.id,
    this.phoneNumber,
    this.areaCode,
    this.createMaximumStore,
    this.phoneVerifiedAt,
    this.email,
    this.emailVerifiedAt,
    this.name,
    this.dateOfBirth,
    this.avatarImage,
    this.score,
    this.sex,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? phoneNumber;
  String? areaCode;
  int? createMaximumStore;
  dynamic phoneVerifiedAt;
  String? email;
  dynamic emailVerifiedAt;
  String? name;
  dynamic dateOfBirth;
  String? avatarImage;
  dynamic score;
  dynamic sex;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"] == null ? null : json["id"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        areaCode: json["area_code"] == null ? null : json["area_code"],
        createMaximumStore: json["create_maximum_store"] == null
            ? null
            : json["create_maximum_store"],
        phoneVerifiedAt: json["phone_verified_at"],
        email: json["email"] == null ? null : json["email"],
        emailVerifiedAt: json["email_verified_at"],
        name: json["name"] == null ? null : json["name"],
        dateOfBirth: json["date_of_birth"],
        avatarImage: json["avatar_image"] == null ? null : json["avatar_image"],
        score: json["score"],
        sex: json["sex"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );
}
