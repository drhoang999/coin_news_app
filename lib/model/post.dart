import 'dart:convert';

class Post {
  Post(
      {this.id,
      this.title,
      this.summary,
      this.fromLink,
      this.thumbnai,
      this.timeGet,
      this.timeWrite,
      this.category,
      this.from,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.description,
      this.isFavorite});

  int? id;
  String? title;
  String? summary;
  String? fromLink;
  String? thumbnai;
  DateTime? timeGet;
  DateTime? timeWrite;
  String? category;
  String? from;
  int? status;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? description;
  bool? isFavorite;

  factory Post.fromRawJson(String str) => Post.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Post.fromJson(Map<String, dynamic> json) => Post(
      id: json["id"],
      title: json["title"],
      summary: json["summary"],
      fromLink: json["from_link"],
      thumbnai: json["thumbnai"],
      timeGet: DateTime.parse(json["time_get"]),
      timeWrite: DateTime.parse(json["time_write"]),
      category: json["category"],
      from: json["from"],
      status: json["status"],
      createdAt: DateTime.parse(json["created_at"]),
      updatedAt: DateTime.parse(json["updated_at"]),
      description: json["description"],
      isFavorite: json['is_favorite']);

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "summary": summary,
        "from_link": fromLink,
        "thumbnai": thumbnai,
        "time_get": timeGet!.toIso8601String(),
        "time_write": timeWrite!.toIso8601String(),
        "category": category,
        "from": from,
        "status": status,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "description": description,
      };
}
