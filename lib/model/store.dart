import 'user.dart';

class Store {
  Store({
    this.id,
    this.name,
    this.storeCode,
    this.dateExpried,
    this.address,
    this.userId,
    this.idTypeOfStore,
    this.createdAt,
    this.updatedAt,
    this.logoUrl,
    this.hasUploadStore,
    this.linkGooglePlay,
    this.linkAppleStore,
    this.totalProducts,
    this.totalProductCategories,
    this.totalPosts,
    this.totalPostCategories,
    this.totalCustomers,
    this.totalOrders,
    this.user,
  });

  int? id;
  String? name;
  String? storeCode;
  DateTime? dateExpried;
  String? address;
  int? userId;
  int? idTypeOfStore;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? logoUrl;
  bool? hasUploadStore;
  String? linkGooglePlay;
  String? linkAppleStore;
  int? totalProducts;
  int? totalProductCategories;
  int? totalPosts;
  int? totalPostCategories;
  int? totalCustomers;
  int? totalOrders;
  User? user;

  factory Store.fromJson(Map<String, dynamic> json) => Store(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        storeCode: json["store_code"] == null ? null : json["store_code"],
        dateExpried: json["date_expried"] == null
            ? null
            : DateTime.parse(json["date_expried"]),
        address: json["address"] == null ? null : json["address"],
        userId: json["user_id"] == null ? null : json["user_id"],
        idTypeOfStore:
            json["id_type_of_store"] == null ? null : json["id_type_of_store"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        logoUrl: json["logo_url"] == null ? null : json["logo_url"],
        hasUploadStore:
            json["has_upload_store"] == null ? null : json["has_upload_store"],
        linkGooglePlay:
            json["link_google_play"] == null ? null : json["link_google_play"],
        linkAppleStore:
            json["link_apple_store"] == null ? null : json["link_apple_store"],
        totalProducts:
            json["total_products"] == null ? null : json["total_products"],
        totalProductCategories: json["total_product_categories"] == null
            ? null
            : json["total_product_categories"],
        totalPosts: json["total_posts"] == null ? null : json["total_posts"],
        totalPostCategories: json["total_post_categories"] == null
            ? null
            : json["total_post_categories"],
        totalCustomers:
            json["total_customers"] == null ? null : json["total_customers"],
        totalOrders: json["total_orders"] == null ? null : json["total_orders"],
        user: json["user"] == null ? null : User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "store_code": storeCode == null ? null : storeCode,
        "date_expried":
            dateExpried == null ? null : dateExpried!.toIso8601String(),
        "address": address == null ? null : address,
        "user_id": userId == null ? null : userId,
        "id_type_of_store": idTypeOfStore == null ? null : idTypeOfStore,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "logo_url": logoUrl == null ? null : logoUrl,
        "has_upload_store": hasUploadStore == null ? null : hasUploadStore,
        "link_google_play": linkGooglePlay == null ? null : linkGooglePlay,
        "link_apple_store": linkAppleStore == null ? null : linkAppleStore,
        "total_products": totalProducts == null ? null : totalProducts,
        "total_product_categories":
            totalProductCategories == null ? null : totalProductCategories,
        "total_posts": totalPosts == null ? null : totalPosts,
        "total_post_categories":
            totalPostCategories == null ? null : totalPostCategories,
        "total_customers": totalCustomers == null ? null : totalCustomers,
        "total_orders": totalOrders == null ? null : totalOrders,
      };
}
