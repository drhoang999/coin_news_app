import 'package:flutter/material.dart';

ThemeData darkGreen = ThemeData(
    accentColor: Colors.orange,
    brightness: Brightness.dark,
    primaryColor: Color(0xfff36745),
    buttonTheme: ButtonThemeData(
      buttonColor: Color(0xffef874e),
      disabledColor: Colors.grey,
    ));
ThemeData lightGreen = ThemeData(
    accentColor: Colors.orangeAccent,
    primaryTextTheme: TextTheme(
        headline6: TextStyle(
            color: Colors.white
        ),
      subtitle2: TextStyle(
          color: Colors.white
      ),
    ),
    brightness: Brightness.light,
    primaryColor: Color(0xffd77d46),
    buttonTheme: ButtonThemeData(
      buttonColor: Color(0xffd77d46),
      disabledColor: Colors.grey,
    ));
ThemeData darkBlue = ThemeData(
    accentColor: Colors.pink,
    brightness: Brightness.dark,
    primaryColor: Colors.red,
    buttonTheme: ButtonThemeData(
      buttonColor: Colors.red,
      disabledColor: Colors.grey,
    ));
ThemeData lightBlue = ThemeData(
    accentColor: Colors.pink,
    brightness: Brightness.light,
    primaryColor: Colors.red,
    buttonTheme: ButtonThemeData(
      buttonColor: Colors.red,
      disabledColor: Colors.grey,
    ));

var LIST_THEME_DATA = [ lightGreen,darkGreen, darkBlue, lightBlue];
