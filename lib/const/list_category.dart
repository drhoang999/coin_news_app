List<CategoryPost> LIST_CATEGORY_POST = [
  CategoryPost("all", "https,//tuoitre.vn/thoi-su/trang-1.htm", "https://i.imgur.com/bVz8oVC.jpeg"),
  CategoryPost("bitcoin", "https,//tuoitre.vn/thoi-su/trang-1.htm","https://i.imgur.com/4Lh4LzQ.jpeg"),
  CategoryPost("blockchain", "https,//tuoitre.vn/the-gioi/trang-1.htm","https://i.imgur.com/ty6hKQV.jpg"),
  CategoryPost("ethereum", "https,//tuoitre.vn/phap-luat/trang-1.htm","https://i.imgur.com/feanZYa.jpeg"),
  CategoryPost("defi", "https,//tuoitre.vn/kinh-doanh/trang-1.htm","https://i.imgur.com/webiCxa.png"),
  CategoryPost("altcoins", "https,//congnghe.tuoitre.vn/","https://i.imgur.com/eObwDmJ.jpg"),
  CategoryPost("regulation", "https,//dulich.tuoitre.vn/di-choi.htm","https://i.imgur.com/S0RKPQp.jpeg"),
  CategoryPost("nft", "https,//tuoitre.vn/nhip-song-tre/trang-1.htm","https://i.imgur.com/oZna5xr.jpeg"),
  CategoryPost("metaverse", "https,//tuoitre.vn/van-hoa/trang-1.htm","https://i.imgur.com/WYydhQZ.jpeg"),
 ];

class CategoryPost {
  String name;
  String link;
  String image;

  CategoryPost(this.name, this.link, this.image);
}
