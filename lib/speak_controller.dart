import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:coinnews24h/component/toast/saha_alert.dart';
import 'package:coinnews24h/data/repository/repository_manager.dart';
import 'package:coinnews24h/model/post.dart';
import 'package:html/parser.dart' as htmlparser;
import 'package:html/dom.dart' as dom;
import 'package:clipboard/clipboard.dart';
import 'const/const_database_shared_preferences.dart';

enum StatusSpeak { None, Loading, Playing, Error, Pause, Stop }
enum Gender { FEMALE, MALE }

class SpeakController extends GetxController {
  Rx<StatusSpeak> statusSpeak = Rx<StatusSpeak>(StatusSpeak.None);

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  var listDescription = [];
  AudioPlayer audioPlugin = AudioPlayer();
  var miliCurrent = 0.obs;
  var miliMax = 0.obs;

  var gender = Rx<Gender>(Gender.FEMALE);
  var name = "".obs;
  Post? post;
  String? pathCurrent = "";
  int? idCurrent;

  void onStop() {
    miliCurrent.value = 0;
    audioPlugin.stop();
    statusSpeak.value = StatusSpeak.None;
  }

  Future<void> getSave() async {
    var s = await _prefs;
    var genderS = s.getString(CURRENT_GENDER);
    if (genderS == "FEMALE")
      gender.value = Gender.FEMALE;
    else
      gender.value = Gender.MALE;
  }

  SpeakController() {
    audioPlugin.onAudioPositionChanged.listen((event) {
      miliCurrent.value = event.inMilliseconds;
    });
    audioPlugin.onDurationChanged.listen((event) {
      miliMax.value = event.inMilliseconds;
    });
  }

  void addNewPost(Post post) {
    this.post = post;
    if (audioPlugin.state == PlayerState.PLAYING) {
      statusSpeak.value = StatusSpeak.Playing;
    } else {
      statusSpeak.value = StatusSpeak.None;
    }
  }

  void onSeek(Duration position) async {
    await audioPlugin.seek(position);
  }

  void playNewPost() async {
    if (audioPlugin.state == PlayerState.PLAYING) {
      statusSpeak.value = StatusSpeak.Pause;
      await audioPlugin.pause();

      return;
    } else {

      var textSpeech ="";

      statusSpeak.value = StatusSpeak.Loading;
      dom.Document document = htmlparser.parse(post!.description);

      listDescription.forEach((des) {
        dom.Document document = htmlparser.parse(des);
        textSpeech = textSpeech +  document.body!.text;
      });
      if (document.body == null) {
        SahaAlert.showError(
            message: "Không thể lấy nội dung bài viết xin thử lại");
        statusSpeak.value = StatusSpeak.None;
      }

      if (post == null) {
        SahaAlert.showError(message: "Không tìm thấy bài viết");
        statusSpeak.value = StatusSpeak.None;
        return;
      }


      if (pathCurrent != null && idCurrent == post!.id) {
        await audioPlugin.play(pathCurrent!, isLocal: true);
        statusSpeak.value = StatusSpeak.Playing;
        return;
      }

      try {
        textSpeech = textSpeech.replaceAll("\"", "");
        textSpeech = textSpeech.replaceAll("  ", "");
        textSpeech = textSpeech.replaceAll('"', '\\"');
        textSpeech = textSpeech.replaceAll("'", '');

        if (textSpeech.length > 4999) {
          textSpeech = textSpeech.substring(0, 4999);
        }

        audioPlugin.stop();
        var res = await RepositoryManager.textToSpeechRepository.speech(
            textSpeech, (gender.value == Gender.FEMALE) ? "FEMALE" : "MALE");
        final String audioContent = res!.audioContent!;

        if (audioContent == null) return;
        final bytes =
            Base64Decoder().convert(audioContent, 0, audioContent.length);
        final dir = await getTemporaryDirectory();
        final file = File('${dir.path}/wavenet.mp3');

        if (await file.exists()) {
          await file.delete();
        }

        await file.writeAsBytes(bytes);
        pathCurrent = file.path;
        idCurrent = post!.id;
        await audioPlugin.play(file.path, isLocal: true);
        statusSpeak.value = StatusSpeak.Playing;
      } catch (err) {
        SahaAlert.showError(message: "Không thể xử lý xin thử lại1");
        statusSpeak.value = StatusSpeak.Error;
      }
    }
  }

  void onChooseGender(String str) async {
    var s = await _prefs;
    s.setString(CURRENT_GENDER, str);

    await getSave();
    pathCurrent = null;
    statusSpeak.value = StatusSpeak.None;
    await audioPlugin.stop();

    playNewPost();
  }

  void onSpeak() {}
}
