import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'loading_container.dart';

class PostItemLoadingWidget extends StatelessWidget {
  const PostItemLoadingWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SahaLoadingContainer(
              height: 70,
              width: 120,
              gloss: 0.5,
            ),
            SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: 5,
                    width: Get.width * 0.7,
                    child: SahaLoadingContainer(
                      width: 70,
                      height: 20,
                      gloss: 0.5,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SahaLoadingContainer(
                    width: Get.width * 0.3,
                    height: 5,
                    gloss: 0.5,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
