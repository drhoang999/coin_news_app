import 'dart:io';
import 'dart:convert';

import 'package:coinnews24h/const/const.dart';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart';
import 'package:either_dart/either.dart';
import 'package:http/http.dart' as http;

import 'package:web_socket_channel/io.dart';

import 'rest/enums.dart';

class BinanceFuture {
  final String endpoint = "fapi.binance.com";
  final String wsEndpoint = "wss://fstream.binance.com";
  final String prefix = "fapi/v1";
  int timestampDifference = 0;
  String? _apiKey;
  String? _apiSecret;

  BinanceFuture({
    String? key,
    String? secret,
  });

  /// Call this function if you keep getting an error about server time
  Future<bool> syncWithServerTime() async {
    var response = await sendRequest(
      path: '$prefix/time',
      type: RequestType.GET,
    );
    if (response.isRight) {
      int serverTime = response.right['serverTime'];
      timestampDifference = DateTime.now().millisecondsSinceEpoch - serverTime;
      return true;
    } else {
      return false;
    }
  }

  /// Helper function to perform any kind of request to Binance API
  Future<Either<String, dynamic>> sendRequest({
    required String path,
    required RequestType type,
    bool keyRequired = false,
    bool signatureRequired = false,
    bool timestampRequired = false,
    Map<String, String>? params,
  }) async {
    params ??= {};
    if (timestampRequired)
      params['timestamp'] =
          (DateTime.now().millisecondsSinceEpoch - timestampDifference)
              .toString();

    if (signatureRequired) {
      _apiSecret = SECRET;
      if (_apiSecret == null) {
        print("Missing API Secret Key");
        return const Left("Missing API Secret Key");
      }
      print(params);
      var tempUri = Uri.https('', '', params);
      String queryParams = tempUri.toString().substring(7);
      List<int> messageBytes = utf8.encode(queryParams);
      List<int> key = utf8.encode(_apiSecret!);
      Hmac hmac = Hmac(sha256, key);
      Digest digest = hmac.convert(messageBytes);
      String signature = hex.encode(digest.bytes);
      params['signature'] = signature;
    }

    Map<String, String> header = {};
    //header[HttpHeaders.contentTypeHeader] = "application/x-www-form-urlencoded";
    if (keyRequired) {
      _apiKey = API_KEY;
      if (_apiKey == null) {
        print("Missing API Key");
        return const Left("Missing API Key");
      }
      header["X-MBX-APIKEY"] = _apiKey!;
    }

    final uri = Uri.https(endpoint, path, params);
    http.Response? response;

    switch (type) {
      case RequestType.GET:
        response = await http.get(
          uri,
          headers: keyRequired ? header : null,
        );
        break;
      case RequestType.POST:
        response = await http.post(
          uri,
          headers: header,
        );
        break;
      case RequestType.DELETE:
        response = await http.delete(
          uri,
          headers: header,
        );
        break;
      case RequestType.PUT:
        response = await http.put(uri);
        break;
      default:
    }

    // print(response!.body);
    final result = jsonDecode(response!.body);
    printWrapped(result.toString());

    if (result is Map) {
      if (result.containsKey("code") && result['code'] != 200) {
        print( "Binance API returned error ${result["code"]} : ${result["msg"]}");
        return Left(
            "Binance API returned error ${result["code"]} : ${result["msg"]}");
      }
    }

    return Right(result);
  }

  void printWrapped(String text) {
    final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((match) => print(match.group(0)));
  }

  /// Helper function to subscribe to any websocket stream from Binance API
  IOWebSocketChannel subscribe(String channel, {String? endpoint}) {

    var path2 = '${endpoint == null ? wsEndpoint : endpoint}/ws/$channel';
    print( path2);

    return IOWebSocketChannel.connect(
      path2,
      pingInterval: const Duration(minutes: 5),
    );
  }

  Map toMap(json) => jsonDecode(json);
  List<Map> toList(json) => List<Map>.from(jsonDecode(json));
}
