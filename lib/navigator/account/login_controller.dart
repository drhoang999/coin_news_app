import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:coinnews24h/component/toast/saha_alert.dart';
import 'package:coinnews24h/data/repository/repository_manager.dart';
import 'package:coinnews24h/utils/user_info.dart';

class LoginController extends GetxController {
  var phoneOrMail = TextEditingController();
  var pass = TextEditingController();

  var handing = false.obs;
  void onLogin() async {
    handing.value = true;
    try {
      var loginData = await RepositoryManager.loginRepository
          .login(phone: phoneOrMail.text,email: phoneOrMail.text, pass: pass.text);

       UserInfo().setData(loginData!);

      await UserInfo().setToken(loginData.token);
      await UserInfo().hasLogged();
      SahaAlert.showSuccess(message: "Logged in successfully");
    } catch (err) {
      SahaAlert.showError(message: err.toString());
    }

    handing.value = false;
  }
}
