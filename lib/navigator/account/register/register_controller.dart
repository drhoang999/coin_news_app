import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:coinnews24h/component/toast/saha_alert.dart';
import 'package:coinnews24h/data/repository/repository_manager.dart';

class RegisterController extends GetxController {
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController pass = TextEditingController();

  var handing = false.obs;

  void onRegister() async {
    handing.value = true;

    try {
      await RepositoryManager.registerRepository.register(
          pass: pass.text,
          phone: phone.text,
          email: email.text,
          name: name.text);

      SahaAlert.showSuccess(message: "Sign Up Success");

      Get.back(result: {
        // "phone": phone.text,
        // "pass": pass.text,
      });

    } catch (err) {
      SahaAlert.showError(message: err.toString());
    }

    handing.value = false;
  }
}
