import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:coinnews24h/component/button/saha_button.dart';
import 'package:coinnews24h/component/loading/loading_full_screen.dart';
import 'package:coinnews24h/component/text_field/sahashopTextField.dart';

import 'register_controller.dart';

class RegisterScreen extends StatelessWidget {

  RegisterController registerController = new RegisterController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Sign up"),
      ),
      body: Obx(
          ()=> Stack(
            children: [
              SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: 100,
                    ),
                    SahaTextField(
                      labelText: 'Name',
                      controller: registerController.name,
                      icon: Icon(Ionicons.mail_outline),
                    ),
                    SahaTextField(
                      labelText: 'Email',
                      controller: registerController.email,
                      icon: Icon(Ionicons.mail_outline),
                    ),
                    SahaTextField(
                      labelText: 'Phone number',
                      controller: registerController.phone,
                      icon: Icon(Ionicons.call_outline),
                    ),
                    SahaTextField(
                      labelText: 'Password',
                      controller: registerController.pass,
                      obscureText: true,
                      icon: Icon(Ionicons.lock_closed_outline),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SahaButtonFullParent(
                      text: "Sign up",
                      color: Theme.of(context).primaryColor,
                      onPressed: () {
                        registerController.onRegister();
                      },
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Row(
                      children: [
                        Text("Do you already have an account?"),
                        Spacer(),
                        InkWell(
                          onTap: (){
                            Get.back();
                          },
                          child: Text(
                            "Login",
                            style: TextStyle(color: Colors.blueAccent),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
        ),

              registerController.handing.value ?
              SahaLoadingFullScreen()
                  :
                  Container()
            ],
          ),
      ),
    );
  }
}