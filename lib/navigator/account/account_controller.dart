import 'package:get/get.dart';
import 'package:coinnews24h/data/remote/response-request/profile/profile_response.dart';
import 'package:coinnews24h/data/repository/repository_manager.dart';
import 'package:coinnews24h/data_app_controller.dart';
import 'package:coinnews24h/utils/user_info.dart';

class AccountController extends GetxController {
  var handing = false.obs;
  var dataProfile = DataProfile().obs;

  DataAppController dataAppController = Get.find();
  AccountController() {
    getProfile();
  }
  void getProfile() async {
    handing.value = true;
    if (dataAppController.hasLogin.value ) {
      handing.value = false;
      try {
        var data = await RepositoryManager.profileRepository.getProfile();

        dataProfile.value = data!;
      } catch (err) {}
    }

    handing.value = false;
  }
}
