import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:coinnews24h/component/button/saha_button.dart';
import 'package:coinnews24h/component/loading/loading_full_screen.dart';
import 'package:coinnews24h/component/text_field/sahashopTextField.dart';
import 'package:coinnews24h/data_app_controller.dart';
import 'package:coinnews24h/utils/user_info.dart';

import 'account_controller.dart';
import 'login_controller.dart';
import 'profile.dart';
import 'register/register.dart';

class AccountScreen extends StatelessWidget {
  LoginController loginController = new LoginController();

  DataAppController dataAppController = Get.find();
  @override
  Widget build(BuildContext context) {


    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Account"),
        ),
        body: Obx(
          () => dataAppController.hasLogin.value == true
              ? Profile()
              : Stack(
                  children: [
                    SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(25.0),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 100,
                            ),
                            Container(
                              color: Colors.white,
                              child: Image.asset(
                                "assets/logo.png",
                                height: 150,
                                fit: BoxFit.cover,
                                alignment: Alignment.center,
                              ),
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            SahaTextField(
                              labelText: 'Email or Phone number',
                              controller: loginController.phoneOrMail,
                              icon: Icon(Ionicons.mail_outline),
                            ),
                            SahaTextField(
                              labelText: 'Password',
                              controller: loginController.pass,
                              obscureText: true,
                              icon: Icon(Ionicons.mail_outline),
                            ),
                            SahaButtonFullParent(
                              text: "Login",
                              color: Theme.of(context).primaryColor,
                              onPressed: () {
                                loginController.onLogin();
                              },
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                // Text("Bạn chưa có tài khoản"),
                                // Spacer(),
                                InkWell(
                                  onTap: () {
                                    Get.to(RegisterScreen())!.then((value) {
                                      if (value is Map) {
                                        if (value['phone'] != null &&
                                            value['pass'] != null) {
                                          loginController.pass.text =
                                          value['pass'];
                                          loginController.phoneOrMail.text =
                                          value['phone'];
                                        }
                                      }
                                    });
                                  },
                                  child: Text(
                                    "Sign up",
                                    style: TextStyle(color: Colors.blueAccent),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    loginController.handing.value
                        ? SahaLoadingFullScreen()
                        : Container()
                  ],
                ),
        ));
  }


}
