import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:coinnews24h/component/dialog/dialog.dart';
import 'package:coinnews24h/navigator/favorite/favorite.dart';
import 'package:coinnews24h/navigator/watched/post_watched.dart';
import 'package:coinnews24h/utils/user_info.dart';

import 'account_controller.dart';

class Profile extends StatelessWidget {
  AccountController accountController = AccountController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Obx(
      () => Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                children: [
                  CircleAvatar(
                    backgroundColor: Colors.grey.withOpacity(0.3),
                    radius: 35,
                    child: Icon(
                      Ionicons.person_circle_outline,
                      size: 40,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(accountController.dataProfile.value.name == null
                      ? "Hi!"
                      : accountController.dataProfile.value.name!),
                  Spacer(),
                  buildItemOption(Icon(Icons.logout), "Log out", onTap: () {
                    SahaDialogApp.showDialogYesNo(
                        mess: "You want to sign out?",
                        onOK: () {
                          UserInfo().logout();
                        });
                  })
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(color: Colors.grey.withOpacity(0.1)),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                            child: buildItemOption(
                                Icon(Ionicons.glasses_outline,

                                ), "Read recently",onTap: (){
                              Get.to(PostWatchedWidget());
                            }),),
                        Expanded(
                            child: buildItemOption(
                                Icon(Ionicons.bookmark_outline,),
                                "Marked",
                            onTap: (){
                                  Get.to(FavoriteWidget());
                            }
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildItemOption(Icon icon, String title, {Function? onTap}) {
    return InkWell(
      onTap: () {
        if (onTap != null) onTap();
      },
      child: Row(
        children: [
          CircleAvatar(
            child: icon,
            backgroundColor: Theme.of(Get.context!).primaryColor,
          ),
          SizedBox(
            width: 5,
          ),
          Text(title)
        ],
      ),
    );
  }
}
