import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:coinnews24h/component/button/saha_button.dart';
import 'package:coinnews24h/component/loading/loading_widget.dart';
import 'package:coinnews24h/component/text_field/sahashopTextField.dart';
import 'package:coinnews24h/component/text_field/text_field_input_otp.dart';
import 'package:coinnews24h/utils/keyboard.dart';
import 'package:coinnews24h/utils/phone_number.dart';

import 'reset_password_controller.dart';

class ResetPasswordScreen extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<ResetPasswordScreen> {
  ResetPasswordController resetPasswordController = ResetPasswordController();
  final _formKey = GlobalKey<FormState>();

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (resetPasswordController.newPassInputting.value == true) {
        return buildNewPasswordInputScreen();
      }
      return buildNumInputScreen();
    });
  }

  Widget buildNumInputScreen() {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lấy lại mật khẩu"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: Column(
              children: [
                SahaTextField(
                  controller:
                      resetPasswordController.textEditingControllerNumberPhone,
                  onChanged: (value) {},
                  autoFocus: true,
                  withAsterisk: true,
                  validator: (value) {
                    if (value!.length < 1) {
                      return 'You did not enter a phone number';
                    }
                    return PhoneNumberValid.validateMobile(value);
                  },
                  textInputType: TextInputType.number,
                  obscureText: false,
                  labelText: "Phone number",
                  hintText: "Please enter your phone number",
                  icon: Icon(Icons.email),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8, right: 8),
                  child: Divider(
                    height: 1,
                    color: Colors.grey,
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                SahaButtonSizeChild(
                    width: 200,
                    text: "Next",
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        _formKey.currentState!.save();
                        KeyboardUtil.hideKeyboard(context);
                        resetPasswordController.newPassInputting.value = true;
                      }
                    }),
                Spacer(),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
          resetPasswordController.resting.value ||
                  resetPasswordController.checkingHasPhone.value
              ? Container(
                  width: Get.width,
                  height: Get.height,
                  child: SahaLoadingWidget(),
                )
              : Container()
        ],
      ),
    );
  }

  Widget buildNewPasswordInputScreen() {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lấy lại mật khẩu"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          onPressed: () {
            resetPasswordController.newPassInputting.value = false;
          },
        ),
      ),
      body: Obx(
        () => Stack(
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [
                  Container(
                    child: Text(
                        "Enter OTP code and enter new password to recover password"),
                    decoration: BoxDecoration(color: Colors.grey[300]),
                    padding: EdgeInsets.all(20),
                  ),
                  TextFieldInputOtp(
                    numberPhone: resetPasswordController
                        .textEditingControllerNumberPhone.text,
                    autoFocus: true,
                    onChanged: (va) {
                      resetPasswordController.otp = va;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SahaTextField(
                    controller:
                        resetPasswordController.textEditingControllerNewPass,
                    onChanged: (value) {},
                    autoFocus: true,
                    validator: (value) {
                      if (value!.length < 6) {
                        return 'New password must be more than 6 characters';
                      }
                      return null;
                    },
                    textInputType: TextInputType.emailAddress,
                    obscureText: true,
                    withAsterisk: true,
                    labelText: "New password",
                    hintText: "Please enter a new password",
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8),
                    child: Divider(
                      height: 1,
                      color: Colors.grey,
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  SahaButtonSizeChild(
                      width: 200,
                      text: "Next",
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          _formKey.currentState!.save();
                          KeyboardUtil.hideKeyboard(context);
                          resetPasswordController.onReset();
                        }
                      }),
                  Spacer(),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
            resetPasswordController.resting.value
                ? Container(
                    width: Get.width,
                    height: Get.height,
                    child: SahaLoadingWidget(),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
