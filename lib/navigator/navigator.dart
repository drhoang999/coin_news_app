import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:coinnews24h/navigator/account/account.dart';

import '../main.dart';

import 'form_bonus/form_bonus.dart';
import 'posts/posts_screen.dart';
import 'posts/view_post/view_post.dart';
import 'setting/setting_screen.dart';
import 'watched/post_watched.dart';

class NavigatorScreen extends StatefulWidget {
  NavigatorScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return NavigatorScreenState();
  }
}

class NavigatorScreenState extends State<NavigatorScreen> {
  int selectedIndex = 0;

  List<Widget> listPage = [
    PostsScreen(),
    FormBonusScreen(),
    SettingScreen(),
    AccountScreen()
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) => {
          if (GlobalVariable.postId != null)
            {
              Navigator.push(
                  GlobalVariable.navState.currentContext!,
                  MaterialPageRoute(
                      builder: (context) =>
                          ViewPostScreen(postId: GlobalVariable.postId)))
            }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: this.getBody(),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: this.selectedIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Ionicons.home_outline),
            label: "Trang nhất",
          ),
          BottomNavigationBarItem(
            icon: Icon(Ionicons.cash_outline),
            label: "Nhận thưởng",
          ),
          BottomNavigationBarItem(
            icon: Icon(Ionicons.settings_outline),
            label: "Settings",
          ),
          BottomNavigationBarItem(
            icon: Icon(Ionicons.person_outline),
            label: "Cá nhân",
          )
        ],
        onTap: (int index) {
          this.onTapHandler(index);
        },
      ),
    );
  }

  Widget getBody() {
    return listPage[selectedIndex];
  }

  void onTapHandler(int index) {
    this.setState(() {
      this.selectedIndex = index;
    });
  }
}

class MyContacts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: Text("Contacts"));
  }
}
