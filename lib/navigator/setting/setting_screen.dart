import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:coinnews24h/const/theme/theme_data.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../data_app_controller.dart';
import 'setting_controller.dart';

class SettingScreen extends StatelessWidget {
  DataAppController dataAppController = Get.find();
  SettingController settingController = SettingController();
  @override
  Widget build(BuildContext context) {
    settingController.getVer();
    settingController.getConfigNotification();
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(8),
            child: Text(
              "Settings",
              style: TextStyle(color: Colors.grey),
            ),
          ),
          InkWell(
            onTap: () {
              showModalBottomSheet(
                  context: context,
                  builder: (builder) {
                    return new Container(
                      height: 150.0,
                      color: Colors
                          .transparent, //could change this to Color(0xFF737373),
                      //so you don't have to change MaterialApp canvasColor
                      child: new Container(
                          decoration: new BoxDecoration(
                              color: Colors.white,
                              borderRadius: new BorderRadius.only(
                                  topLeft: const Radius.circular(10.0),
                                  topRight: const Radius.circular(10.0))),
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: LIST_THEME_DATA
                                .map((e) => Center(
                                      child: InkWell(
                                        onTap: () {
                                          dataAppController.changeTheme(
                                              e, LIST_THEME_DATA.indexOf(e));
                                        },
                                        child: Stack(
                                          alignment: Alignment.topCenter,
                                          children: [
                                            Container(
                                              decoration: BoxDecoration(
                                                  color: e.primaryColor,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(8))),
                                              padding: EdgeInsets.all(15),
                                              margin: EdgeInsets.all(15),
                                              height: 100,
                                              width: 200,
                                            ),
                                            Container(
                                                color: Colors.grey
                                                    .withOpacity(0.1),
                                                child: Text(e.brightness ==
                                                        Brightness.dark
                                                    ? "Dark"
                                                    : "Light",
                                                style: TextStyle(
                                                  color: Colors.black87
                                                ),
                                                ))
                                          ],
                                        ),
                                      ),
                                    ))
                                .toList(),
                          )),
                    );
                  });
            },
            child: Container(
              padding: EdgeInsets.all(8),
              child: Row(
                children: [
                  Icon(Ionicons.color_palette_outline),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Theme"),
                  Spacer(),
                  Text("Theme"),
                ],
              ),
            ),
          ),
          Divider(),

          InkWell(
            onTap: () {

            },
            child: Obx(
    ()=> Container(
                padding: EdgeInsets.all(8),
                child: Row(
                  children: [
                    Icon(Ionicons.notifications_outline),
                    SizedBox(
                      width: 10,
                    ),
                    Text("Notification"),
                    Spacer(),
                   CupertinoSwitch(value: settingController.isAllowNotification.value, onChanged: (v){
                     settingController.onChangeNotificationConfig(v);
                   })
                  ],
                ),
              ),
            ),
          ),
          Divider(),
          InkWell(
            onTap: () {
              launch("mailto:microads.ex@gmail.com");
            },
            child: Container(
              padding: EdgeInsets.all(8),
              child: Row(
                children: [
                  Icon(Ionicons.mail_outline),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Email"),
                  Spacer(),
                  Text("coin.news24h@gmail.com")
                ],
              ),
            ),
          ),

          Divider(),

          InkWell(
            onTap: () {
              launch("tel:09471228789");
            },
            child: Container(
              padding: EdgeInsets.all(8),
              child: Row(
                children: [
                  Icon(Ionicons.call),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Phone number"),
                  Spacer(),
                  Text("+1910.764.8790")
                ],
              ),
            ),
          ),

          Divider(),

          InkWell(
            onTap: () {
               launch("https://crypto.news/");
            },
            child: Container(
              padding: EdgeInsets.all(8),
              child: Row(
                children: [
                  Icon(Ionicons.document_outline),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Article source"),
                  Spacer(),
                  Text("crypto.news | 0918.044.044")
                ],
              ),
            ),
          ),

          Divider(),

          Obx(
            () => settingController.handing.value == true ||
                    settingController.packageInfo == null
                ? Container()
                : Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        "Version: " + settingController.packageInfo!.version),
                  ),
          )
        ],
      ),
    );
  }
}
