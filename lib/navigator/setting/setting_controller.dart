import 'package:get/get.dart';
import 'package:package_info/package_info.dart';
import 'package:coinnews24h/data/repository/repository_manager.dart';
import 'package:coinnews24h/load_data/load_firebase.dart';

class SettingController extends GetxController {
  PackageInfo? packageInfo;

  var handing = true.obs;
  var isAllowNotification = false.obs;

  SettingController() {
    getVer();
    getConfigNotification();
  }

  void getVer() async {
    handing.value = true;
    packageInfo = await PackageInfo.fromPlatform();
    print(packageInfo!.version);
    handing.value = false;
  }

  void onChangeNotificationConfig(bool va) async {
    isAllowNotification.value =va;
    try {
      var data = await RepositoryManager.deviceTokenRepository
          .updateDeviceTokenUser(FCMToken().token, va);
    } catch (err) {
      print("Err getConfigNotification");
    }
    getConfigNotification();
  }

  void getConfigNotification() async {
    try {
      var data =
          await RepositoryManager.deviceTokenRepository.getDeviceTokenUser();
      if (data != null) {
        if (data.active == true) {
          isAllowNotification.value = true;
        } else {
          isAllowNotification.value = false;
        }
      }
    } catch (err) {
      print("Err getConfigNotification");
    }
  }
}
