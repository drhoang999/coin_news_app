import 'package:get/get.dart';
import 'package:coinnews24h/component/toast/saha_alert.dart';
import 'package:coinnews24h/data/repository/repository_manager.dart';
import 'package:coinnews24h/data_app_controller.dart';
import 'package:html/parser.dart' as htmlparser;
import 'package:html/dom.dart' as dom;

import '../../../speak_controller.dart';

class ViewPostController extends GetxController {


  DataAppController dataAppController = Get.find();
  var loading = false.obs;
  var idPost;
  var isFavorite = false.obs;
  var listDescription = [];

  SpeakController speakController = Get.find();
  void onSpeed() {
    speakController.onSpeak();
  }

  ViewPostController(int postId) {
    getPost(postId);
    idPost = postId;
  }


  void splitPost(var description) {
    dom.Document document = htmlparser.parse(description);
    dom.Document document2 = htmlparser.parse(description);

    if (document.getElementById("main-detail-body") != null &&
        document.getElementById("main-detail-body")!.nodes.length > 3) {

      var nodes =  document.getElementById("main-detail-body")!.nodes;
      var nodesHead =  nodes.sublist(0,3);
      //handle head
      document.getElementById("main-detail-body")!.nodes.clear();
      for(var node in nodesHead) {
        document.getElementById("main-detail-body")!.append(node);

      }

      if(document.getElementsByClassName("author").length > 0) {
        document.getElementsByClassName("author")[0].nodes.clear();
      }

      if(document.getElementsByClassName("relate-container").length > 0) {
        document.getElementsByClassName("relate-container")[0].nodes.clear();
      }

      listDescription.add(document.outerHtml);

      //handle end

      var nodes2 =  document2.getElementById("main-detail-body")!.nodes;
      var nodesEnd =  nodes2.sublist(4);

      document2.getElementById("main-detail-body")!.nodes.clear();

      for(var node in nodesEnd) {
        print(node.attributes.toString());
        if(!node.attributes.toString().contains("VCSortableInPreviewMode")) {
          document2.getElementById("main-detail-body")!.append(node);
        }
      }

      if(document2.getElementsByClassName("sapo").length > 0) {
        document2.getElementsByClassName("sapo")[0].nodes.clear();
      }
      if(document2.getElementsByClassName("relate-container").length > 0) {
        document2.getElementsByClassName("relate-container")[0].nodes.clear();
      }
      if(document2.getElementsByClassName("VCSortableInPreviewMode").length > 0) {
        document2.getElementsByClassName("VCSortableInPreviewMode")[0].nodes.clear();
      }

      //document.nodes[1].remove();

      listDescription.add(document2.outerHtml);

    } else {
      listDescription.add(document.outerHtml);
    }

    speakController.listDescription = listDescription;
  }

  void favorite(bool isFavorite) {
    if( dataAppController.hasLogin.value == false) {
      SahaAlert.showError(message: "Sign in to bookmark");
      return;
    }
    RepositoryManager.favoriteRepository
        .favorite(idPost: idPost, isFavorite: isFavorite);
    this.isFavorite.value = isFavorite;
  }

  void getPost(int postId) async {
    loading.value = true;

    var data = await RepositoryManager.postRepository.getOnePost(postId);
    splitPost(data!.data!.description);
    speakController.addNewPost(data.data!);

    speakController.post = data.data;
    if (data != null) {
      print( data.data!.isFavorite!.toString() + "xxxx");
      isFavorite.value = data.data!.isFavorite!;
      onSpeed();
    }
    loading.value = false;
  }
}

