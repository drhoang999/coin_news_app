import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';


const paddingSlider = 35;
const colorTrack = Colors.cyan;

class MusicSliderWidget extends StatelessWidget {
  final double? sliderHeight;
  final int? min;
  final int? max;
  final int? value;
  final fullWidth;
  final Function? onChangeEnd;
  final Function? onChange;
  final Function? onChangeStart;

  MusicSliderWidget(
      {this.sliderHeight = 48,
        this.onChangeStart,
        this.max,
        this.min,
        this.fullWidth = false,
        this.value,
        this.onChangeEnd,
        this.onChange});

  @override
  Widget build(BuildContext context) {
    // print("$value/$min/$max");

    double paddingFactor = .2;
    if (fullWidth) paddingFactor = .3;

    return Container(
      width: fullWidth ? double.infinity : (sliderHeight)! * 5.5,
      height: (sliderHeight),
      decoration: new BoxDecoration(
        borderRadius: new BorderRadius.all(
          Radius.circular((sliderHeight! * .3)),
        ),
      ),
      child: Row(
        children: <Widget>[
          // Container(
          //   width: 70,
          //   height: 3,
          //   color: Colors.cyan,
          // ),
          Expanded(
            child: SliderTheme(
              data: SliderTheme.of(Get.context!).copyWith(
                activeTrackColor: colorTrack,
                inactiveTrackColor: Colors.grey,
                trackHeight: 3.0,
                trackShape: CustomTrackShape(),
                tickMarkShape: RoundSliderTickMarkShape(),
                thumbShape: CustomSliderThumbCircle(
                  thumbRadius: sliderHeight! * .5,
                  min: min,
                  max: max,
                  value: value,
                ),
                overlayShape: RoundSliderOverlayShape(overlayRadius: 1.0),
                overlayColor: Colors.white.withOpacity(.4),
                valueIndicatorColor: colorTrack,
                activeTickMarkColor: colorTrack,

                inactiveTickMarkColor: Colors.red.withOpacity(.7),
              ),
              child: Stack(
                alignment: Alignment.centerLeft,
                children: [
                  Container(
                    height: 2,
                    width: paddingSlider + 1.toDouble(),
                    color: colorTrack,
                  ),
                  Slider(
                    onChangeStart: (va) {
                      if (onChangeStart != null) onChangeStart!(value);
                    },
                    onChangeEnd: (va) {
                      if (onChangeEnd != null) onChangeEnd!(value);
                    },
                    // divisions: max != null && min != null && (max - min) > 0
                    //     ? (max - min)
                    //     : null,
                    // label: timeStringFromMilliseconds(value.floor()) +
                    //     "/" +
                    //     timeStringFromMilliseconds(max.floor()),
                    value: value!.toDouble(),
                    min: min!.toDouble(),
                    max: max!.toDouble(),
                    onChanged: (va) {
                      if (onChange != null) onChange!(va.floor());
                    },
                  ),
                ],
              ),
            ),
          ),
          // Container(
          //   width: 40,
          //   height: 3,
          // ),
        ],
      ),
    );
  }
}

class CustomSliderThumbCircle extends SliderComponentShape {
  final double? thumbRadius;
  final int? min;
  final int? max;
  final int? value;

  const CustomSliderThumbCircle({
    @required this.thumbRadius,
    this.min = 0,
    this.value,
    this.max = 10,
  });

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(thumbRadius!);
  }

  @override
  void paint(
      PaintingContext context,
      Offset center, {
        Animation<double>? activationAnimation,
        Animation<double>? enableAnimation,
        bool? isDiscrete,
        TextPainter? labelPainter,
        RenderBox? parentBox,
        SliderThemeData? sliderTheme,
        TextDirection? textDirection,
        double? value,
        double? textScaleFactor,
        Size? sizeWithOverflow,
      }) {
    final Canvas canvas = context.canvas;

    var paint = Paint()
      ..color = colorTrack //Thumb Background Color
      ..style = PaintingStyle.fill;

    TextSpan span = new TextSpan(
      style: new TextStyle(
        fontSize: thumbRadius,
        fontWeight: FontWeight.w300,
        color: Colors.white, //Text Color of Value on Thumb
      ),
      text: getValue(),
    );

    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr);
    tp.layout();
    Offset textCenter =
    Offset(center.dx - (tp.width / 2), center.dy - (tp.height / 2));

    final double height = 13;
    final double width = 70;
    final double left = center.dx - (width / 2);
    final double top = center.dy - (height / 2);

    canvas.drawRRect(
        RRect.fromRectAndRadius(
            Rect.fromLTWH(left, top, width, height), Radius.circular(15.0)),
        paint);

    tp.paint(canvas, textCenter);
  }

  String getValue() {
    return timeStringFromMilliseconds(value!.floor()) +
        "/" +
        timeStringFromMilliseconds(max!.floor());
  }

  String timeStringFromMilliseconds(int milliseconds) {
    if (milliseconds == null || milliseconds == 0) return "00:00:00";
    String seconds = ((milliseconds / 1000) % 60).floor().toString().padLeft(2, "0");
    String minutes =
    (((milliseconds / (1000 * 60)) % 60)).floor().toString().padLeft(2, "0");
    String hours =
    (((milliseconds / (1000 * 60 * 60)) % 24)).floor().toString().padLeft(2, "0");
    return "$hours:$minutes:$seconds";
  }


}

class CustomTrackShape extends RoundedRectSliderTrackShape {
  Rect getPreferredRect(
      {@required RenderBox? parentBox,
        Offset offset = Offset.zero,
        @required SliderThemeData? sliderTheme,
        bool isEnabled = false,
        bool isDiscrete = false,
        additionalActiveTrackHeight = 1}) {
    final double trackHeight = 0;
    final double trackLeft = offset.dx + paddingSlider.toDouble();
    final double trackTop =
        offset.dy + (parentBox!.size.height - trackHeight) / 2;
    final double trackWidth = parentBox.size.width - 68;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }
}

class VolumeSlider extends StatelessWidget {
  final double? width;
  final double? height;
  final double? volume;
  final Function? onChanged;
  final bool? selected;

  const VolumeSlider({Key? key, this.width, this.height, this.volume,
    this.onChanged, this.selected = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(
          milliseconds: 500
      ),
      width: 500,
      child: Container(
        width: width,
        height: height,
        child: Stack(
          children: [
            CustomPaint(
              painter: VolumeCustom(
                  volume:volume!,
                  buttonBorderColor: Colors.grey,
                  progressColor: Colors.cyan,
                  percentage: 5),
              child: Container(),
            ),
            SliderTheme(
              data: SliderTheme.of(context).copyWith(
                activeTrackColor: Colors.transparent,
                inactiveTrackColor: Colors.transparent,
                trackShape: RoundedRectSliderTrackShape(),
                trackHeight: 4.0,
                thumbShape: RoundSliderThumbShape(enabledThumbRadius: 0.0),
                thumbColor: Colors.transparent,
                overlayColor: Colors.red.withAlpha(32),
                overlayShape: RoundSliderOverlayShape(overlayRadius: 0.0),
                tickMarkShape: RoundSliderTickMarkShape(),
                activeTickMarkColor: Colors.transparent,
                inactiveTickMarkColor: Colors.transparent,
                valueIndicatorShape: PaddleSliderValueIndicatorShape(),
                valueIndicatorColor: Colors.transparent,
                valueIndicatorTextStyle: TextStyle(
                  color: Colors.white,
                ),
              ),
              child: Slider(
                min: 0,
                max: 100,
                onChanged: (va) {

                },
                value: volume ?? 0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class VolumeCustom extends CustomPainter {
  Color? buttonBorderColor;
  Color? progressColor;
  double? percentage;
  double? volume;

  VolumeCustom({
    this.buttonBorderColor,
    this.progressColor,
    this.percentage,
    this.volume =0,
  });
  @override
  void paint(Canvas canvas, Size size) {


    Paint paint = Paint()
      ..color = Colors.grey
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill
      ..strokeWidth = 2;

    Paint paintColorVolumeSelected = Paint()
      ..color = progressColor!
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill
      ..strokeWidth = 2;

    canvas.drawPath(getTrianglePath(size.width, size.height), paint);
    canvas.drawPath(getTriangleChoosePath(size.width, size.height), paintColorVolumeSelected);
  }

  Path getTrianglePath(double x, double y) {
    return Path()
      ..moveTo(0, y - 5)
      ..lineTo(0, y)
      ..lineTo(x, y)
      ..lineTo(x, 0 );
  }

  Path getTriangleChoosePath(double x, double y) {
    final percentVolume = volume!/100;
    return Path()
      ..moveTo(0, y - 5)
      ..lineTo(0, y)
      ..lineTo(x*percentVolume, y)
      ..lineTo(x*percentVolume, (y - percentVolume*y));
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
