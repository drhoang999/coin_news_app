import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:coinnews24h/navigator/symbol/symbol_screen.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:coinnews24h/component/loading/loading_container.dart';
import 'package:coinnews24h/const/list_category.dart';
import 'package:coinnews24h/navigator/account/account.dart';
import 'package:coinnews24h/navigator/form_bonus/form_bonus.dart';
import 'package:coinnews24h/navigator/posts/post_page.dart';
import 'package:coinnews24h/navigator/setting/setting_screen.dart';

class PostsScreen extends StatefulWidget {
  @override
  _PostsScreenState createState() => _PostsScreenState();
}

class _PostsScreenState extends State<PostsScreen>
    with SingleTickerProviderStateMixin {
  late final TabController _tabController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController =
        TabController(length: LIST_CATEGORY_POST.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      // Added
      length: LIST_CATEGORY_POST.length, // Added
      initialIndex: 0, //Added
      child: Scaffold(
        drawer: new Drawer(
          elevation: 0,
          child: Column(
            children: [
              Container(
                height: MediaQuery.of(context).padding.top,
              ),
              Expanded(
                child: ListView.builder(
                    padding: EdgeInsets.zero,
                    itemCount: (LIST_CATEGORY_POST.length / 2).floor(),
                    itemBuilder: (context, index) => Row(
                          children: [
                            Expanded(child: buildItemCate(index * 2)),
                            Expanded(child: buildItemCate(index * 2 + 1)),
                          ],
                        )),
              ),
              Container(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              height: 50,
                              alignment: Alignment.center,
                              margin: EdgeInsets.all(8),
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                border: Border.all( color: Theme.of(context).primaryColor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3)),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Icon(Ionicons.home_outline),
                                  Text("Home" , style: TextStyle(
                                  color: Theme.of(context).primaryColor
                            ),),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => SymbolScreen()));
                            },
                            child: Container(
                              height: 50,
                              alignment: Alignment.center,
                              margin: EdgeInsets.all(8),
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                border: Border.all(  color: Theme.of(context).primaryColor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3)),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Icon(Ionicons.pricetag_outline),
                                  Text("Prices",
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColor
                                  ),),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => SettingScreen()));
                            },
                            child: Container(
                              height: 50,
                              alignment: Alignment.center,
                              margin: EdgeInsets.all(8),
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                border: Border.all( color: Theme.of(context).primaryColor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3)),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text("Settings", style: TextStyle(
                                      color: Theme.of(context).primaryColor
                                  ),),
                                  Icon(Ionicons.settings_outline),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => AccountScreen()));
                            },
                            child: Container(
                              height: 50,
                              alignment: Alignment.center,
                              margin: EdgeInsets.all(8),
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                border: Border.all( color: Theme.of(context).primaryColor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3)),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Icon(Ionicons.person_outline),
                                  Text("Account", style: TextStyle(
                                    color: Theme.of(context).primaryColor
                                  ),),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                height: 15,
              )
            ],
          ),
        ),
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                  leading: Container(),
                  title: Text('News'),
                  pinned: true,
                  floating: true,
                  forceElevated: innerBoxIsScrolled,
                  bottom: PreferredSize(
                    child: Row(
                      children: [
                        Container(
                          color:Color(0xffef874e),
                          child: Padding(
                            padding: const EdgeInsets.only(right: 5),
                            child: IconButton(
                                onPressed: () {
                                  Scaffold.of(context).openDrawer();
                                },
                                icon: Icon(
                                  Ionicons.list,
                                  color: Colors.white,
                                )),
                          ),
                        ),
                        Expanded(
                          child: Material(
                            color: Colors.white,
                            child: TabBar(
                              unselectedLabelColor: Colors.lightBlue[100],
                              labelColor: Theme.of(context).primaryColor,
                              isScrollable: true,
                              tabs: LIST_CATEGORY_POST
                                  .map(
                                    (e) => Tab(text: e.name.capitalize()),
                                  )
                                  .toList(),
                              controller: _tabController,
                            ),
                          ),
                        ),
                      ],
                    ),
                    preferredSize: Size.fromHeight(50),
                  )),
            ];
          },
          body: TabBarView(
            controller: _tabController,
            children: LIST_CATEGORY_POST
                .map((e) => Container(
                    key: new PageStorageKey(e.name),
                    child: PostPageWidget(
                      category: e.name,
                    )))
                .toList(),
          ),
        ),
      ),
    );
  }

  Widget buildItemCate(int index) {
    return InkWell(
      onTap: () {
        _tabController.animateTo(index);
        Navigator.pop(context);
      },
      child: Container(
        height: 70,

        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.black.withOpacity(0.5),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: [
            ImageFiltered(
              imageFilter: ImageFilter.blur(
                  sigmaY: 1,
                  sigmaX: 1), //SigmaX and Y are just for X and Y directions
              child: CachedNetworkImage(
                fit: BoxFit.cover,
                imageUrl: LIST_CATEGORY_POST[index].image,
                placeholder: (context, url) => SahaLoadingContainer(
                  height: 70,

                  gloss: 0.5,
                ),
                errorWidget: (context, url, error) => Container(
                    height: 70, width: 70, color: Colors.grey.withOpacity(0.2)),
              ),
            ),
            Container(
              color: Colors.black.withOpacity(0.5),
            ),
            Text(
              LIST_CATEGORY_POST[index].name.toUpperCase(),
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}
