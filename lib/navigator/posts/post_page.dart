import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:coinnews24h/admob/admob_controller.dart';
import 'package:coinnews24h/admob/widget_item_admob_list.dart';
import 'package:coinnews24h/component/loading/post_item_loading.dart';
import 'package:coinnews24h/navigator/posts/post_page_controller.dart';
import 'package:loadmore/loadmore.dart';
import 'posts_controller.dart';
import 'widget/post_widget.dart';

class PostPageWidget extends StatefulWidget {
  final String? category;

  const PostPageWidget({Key? key, this.category}) : super(key: key);
  @override
  _PostPageWidgetState createState() => _PostPageWidgetState();
}

class _PostPageWidgetState extends State<PostPageWidget>
    with AutomaticKeepAliveClientMixin<PostPageWidget> {
  late PostPageController postsController;

  // AdmobController admobController = Get.find();

  @override
  bool get wantKeepAlive => true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    postsController = new PostPageController(widget.category);
  }

  RefreshController _refreshController = RefreshController();

  String _buildEnglishText(LoadMoreStatus status) {
    String text;
    switch (status) {
      case LoadMoreStatus.fail:
        text = "Error";
        break;
      case LoadMoreStatus.idle:
        text = "Please wait";
        break;
      case LoadMoreStatus.loading:
        text = "Loading more ...";
        break;
      case LoadMoreStatus.nomore:
        text = "End of post";
        break;
      default:
        text = "";
    }
    return text;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Obx(
        () => postsController.isLoadingPost.value == true
            ? ListView.builder(
                padding: EdgeInsets.zero,
                itemBuilder: (context, index) => PostItemLoadingWidget())
            : LoadMore(
                delegate: DefaultLoadMoreDelegate(),
                textBuilder: _buildEnglishText,
                isFinish: postsController.isEndPost.value,
                onLoadMore: () {
                  return postsController.getAllPost();
                },
                child: ListView.builder(
                    padding: EdgeInsets.zero,
                    itemCount: postsController.listPost.length,
                    itemBuilder: (context, index) => index % 5 == 0
                        ? Column(
                          children: [
                            if(index % 5 == 0 && index != 0)  ItemAdMob(),
                            PostWidgetBig(post: postsController.listPost[index]),
                          ],
                        )
                        :
                              PostWidget(post: postsController.listPost[index]),
                          ),
              ),
      ),
    );
  }
}

// SmartRefresher(
// enablePullDown: true,
// enablePullUp: true,
// header: MaterialClassicHeader(),
// footer: CustomFooter(
// builder: (
// BuildContext context,
//     LoadStatus? mode,
// ) {
// Widget body;
// if (mode == LoadStatus.idle) {
// body = Container();
// } else if (mode == LoadStatus.loading) {
// body = CupertinoActivityIndicator();
// } else if (mode == LoadStatus.failed) {
// body = Text("Load Failed!Click retry!");
// } else if (mode == LoadStatus.canLoading) {
// body = Text("Đã hết");
// } else {
// body = Text("Đã xem hết");
// }
// return Container(
// height: 55.0,
// child: Center(child: body),
// );
// },
// ),
// controller: _refreshController,
// onRefresh: () async {
// await postsController.getAllPost(isRefresh: true);
// _refreshController.refreshCompleted();
// },
// onLoading: () async {
// await postsController.getAllPost(isRefresh: false);
// _refreshController.loadComplete();
// },
// child:
