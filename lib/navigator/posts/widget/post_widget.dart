import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:coinnews24h/component/loading/loading_container.dart';
import 'package:coinnews24h/model/post.dart';
import 'package:coinnews24h/navigator/posts/view_post/view_post.dart';
import 'package:coinnews24h/utils/string_utils.dart';

class PostWidget extends StatelessWidget {
  final Post? post;

  const PostWidget({Key? key, this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
      onTap: () {
        Get.to(() => ViewPostScreen(post: post));
      },
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(5.0),
              child: CachedNetworkImage(
                height: 70,
                width: 120,
                fit: BoxFit.cover,
                imageUrl: post!.thumbnai!,
                placeholder: (context, url) => SahaLoadingContainer(
                  height: 70,
                  width: 120,
                  gloss: 0.5,
                ),
                errorWidget: (context, url, error) => Container(
                    height: 70,
                    width: 120,
                    color: Colors.grey.withOpacity(0.2)),
              ),
            ),
            SizedBox(
              width: 5,
            ),
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(post!.title!),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    Text(
                      post!.category!,
                      style: TextStyle(color: Colors.grey, fontSize: 13),
                    ),
                    Spacer(),
                    Column(
                      children: [
                        // Text(
                        //   (post!.from! + "  ").replaceAll(".vn", ""),
                        //   style: TextStyle(color: Colors.grey, fontSize: 11),
                        // ),
                        Text(
                          SahaStringUtils()
                              .displayTimeAgoFromTime(post!.timeWrite!),
                          style: TextStyle(color: Colors.grey, fontSize: 11),
                        ),
                      ],
                    )
                  ],
                )
              ],
            ))
          ],
        ),
      ),
    );
  }
}

class PostWidgetBig extends StatelessWidget {
  final Post? post;

  const PostWidgetBig({Key? key, this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
      onTap: () {
        Get.to(() => ViewPostScreen(post: post));
      },
      child: Container(
        padding: EdgeInsets.all(0),
        width: Get.width,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(0.0),
              child: CachedNetworkImage(
                width: Get.width,
                fit: BoxFit.contain,
                imageUrl: post!.thumbnai!,
                placeholder: (context, url) => SahaLoadingContainer(
                  height: 70,
                  width: 120,
                  gloss: 0.5,
                ),
                errorWidget: (context, url, error) => Container(
                    height: 70,
                    width: 120,
                    color: Colors.grey.withOpacity(0.2)),
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          post!.title!,
                          style: TextStyle(fontSize: 21),
                        ),
                      ),
                    ],
                  ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              post!.category!,
                              style: TextStyle(color: Colors.grey, fontSize: 11),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              SahaStringUtils().displayTimeAgoFromTime(post!.timeWrite!),
                              style: TextStyle(color: Colors.grey, fontSize: 11),
                            ),
                          ],
                        )
                ],
              ),
            ),

            // Container(
            //   height: 30,
            //   child: Row(
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     children: [
            //       Text(post!.title!),
            //       SizedBox(
            //         width: 5,
            //       ),
            //       Text(
            //         post!.category!,
            //         style: TextStyle(color: Colors.grey, fontSize: 13),
            //       ),
            //       Spacer(),
            //       Text( post!.from! + "  ",style: TextStyle(color: Colors.grey, fontSize: 11),),
            //       Text(
            //         SahaStringUtils().displayTimeAgoFromTime(post!.timeWrite!),
            //         style: TextStyle(color: Colors.grey, fontSize: 11),
            //       ),
            //     ],
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
