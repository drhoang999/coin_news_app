import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:coinnews24h/component/toast/saha_alert.dart';
import 'package:coinnews24h/data/repository/repository_manager.dart';

class FormBonusController extends GetxController {
  var isSentOtp = false.obs;
  var sending = false.obs;

  var sent = false.obs;

  var showSuccess = "";
  Timer? _timer;
  var start = 30.obs;

  TextEditingController textEditingControllerEmail =
      new TextEditingController();
  TextEditingController textEditingControllerPhone =
      new TextEditingController();
  TextEditingController textEditingControllerOtp = new TextEditingController();
  TextEditingController textEditingControllerAddress =
      new TextEditingController();

  FormBonusController(){
    getSetting();
  }

  void startTimer() {
    start.value = 30;
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) {
        if (start.value == 0) {
          isSentOtp.value = false;
            timer.cancel();

        } else {
            start--;
        }
      },
    );
  }

  void getSetting() async {
    try {
      var data = await RepositoryManager.configRepository.getConfig();
      showSuccess = data!.successInputForm!;
    } catch (err) {}
  }

  void sendOtp() {
    isSentOtp.value = true;

    try {
      startTimer();

      RepositoryManager.otpRepository
          .sendOTP(phoneNumber: textEditingControllerPhone.text);

    } catch (err) {
      SahaAlert.showError(message: "Có lỗi khi gửi OTP");
    }


  }

  void sendFormBonus() {
    sent.value = false;

    sending.value = true;
    try {
      RepositoryManager.formBonusRepository.sendFormBonus(
          phone: textEditingControllerPhone.text,
          email: textEditingControllerEmail.text,
          address: textEditingControllerAddress.text,
          otp: textEditingControllerOtp.text);

      sent.value = true;

      textEditingControllerEmail.text = "";
      textEditingControllerOtp.text = "";
      textEditingControllerAddress.text = "";
      textEditingControllerPhone.text = "";
    } catch (err) {
      SahaAlert.showError(message: "Có lỗi khi gửi OTP");
    }

    sending.value = false;
  }
}
