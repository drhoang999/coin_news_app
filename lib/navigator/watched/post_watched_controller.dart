import 'package:get/get.dart';
import 'package:coinnews24h/component/toast/saha_alert.dart';
import 'package:coinnews24h/data/repository/repository_manager.dart';
import 'package:coinnews24h/model/post.dart';

class PostWatchedController extends GetxController {
  final String? category;

  var listPost = RxList<Post>();
  var currentPage = 1;
  String? textSearch;
  var isSearch = false.obs;
  var isLoadingPost = false.obs;
  var isLoadMore = false.obs;
  var isEndPost = false.obs;

  PostWatchedController(this.category) {
    getAllPost(isRefresh: true, category: category);
  }

  Future<void> getAllPost(
      {bool? isRefresh, String? textSearch, String? category}) async {
    try {
      if (isRefresh == true) {
        isLoadingPost.value = true;
        currentPage = 1;
        isEndPost.value = false;
        listPost([]);
      }

      if (isEndPost.value == false) {
        var data = await RepositoryManager.postRepository
            .getWatchedPost(currentPage);

        listPost.addAll(data!.data!.data!);

        if (data.data!.nextPageUrl == null) {
          isEndPost.value = true;
        } else {
          currentPage++;
        }

        isLoadingPost.value = false;
        isLoadMore.value = false;
      } else {
        isLoadMore.value = false;
      }
    } catch (err) {
      SahaAlert.showError(message: err.toString());
    }
  }
}
