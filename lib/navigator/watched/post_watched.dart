import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:coinnews24h/component/loading/post_item_loading.dart';
import 'package:coinnews24h/navigator/posts/widget/post_widget.dart';
import 'post_watched_controller.dart';


class PostWatchedWidget extends StatefulWidget {
  final String? category;

  const PostWatchedWidget({Key? key, this.category}) : super(key: key);
  @override
  _PostWatchedWidgetState createState() => _PostWatchedWidgetState();
}

class _PostWatchedWidgetState extends State<PostWatchedWidget>
    with AutomaticKeepAliveClientMixin<PostWatchedWidget> {
  late PostWatchedController postsController;
  @override
  bool get wantKeepAlive => true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    postsController = new PostWatchedController(widget.category);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Watched"),
      ),
      body: Obx(
        () => postsController.isLoadingPost.value == true
            ? ListView.builder(
                padding: EdgeInsets.zero,
                itemBuilder: (context, index) => PostItemLoadingWidget())
            : ListView.builder(
                padding: EdgeInsets.zero,
                itemCount: postsController.listPost.length,
                itemBuilder: (context, index) =>
                    PostWidget(post: postsController.listPost[index])),
      ),
    );
  }
}
