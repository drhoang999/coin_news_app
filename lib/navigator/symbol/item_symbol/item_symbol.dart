import 'package:coinnews24h/utils/date_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'item_symbol_controller.dart';

class ItemSymbolWidget extends StatefulWidget {
  final String symbolInput;

  late ItemSymbolController itemSymbolController;

  ItemSymbolWidget({Key? key, required this.symbolInput}) {
    itemSymbolController = new ItemSymbolController(symbolInput);
  }

  @override
  State<ItemSymbolWidget> createState() => _ItemSymbolWidgetState();
}

class _ItemSymbolWidgetState extends State<ItemSymbolWidget>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          // border: Border.all(color: Colors.white)
          ),
      padding: EdgeInsets.only(bottom: 5),
      child: Obx(() {
        var wsKline = widget.itemSymbolController.wsKline.value.wsKlineEvent;
        return wsKline == null
            ? Container()
            : Container(
          padding: EdgeInsets.all(12),
        
                child: Row(
                  children: [
                    Expanded(flex: 2, child: Text(wsKline.symbol)),
                    Expanded(
                        flex: 3, child: Text(wsKline.kline.close.toString() + "\$",
                    textAlign: TextAlign.center,
                    )),
                    Container(
                      padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                            color: widget.itemSymbolController.wsKline.value
                                        .percentInterval ==
                                    0
                                ? Colors.grey
                                : (widget.itemSymbolController.wsKline.value
                                            .percentInterval >
                                        0
                                    ? Colors.green
                                    : Colors.red)),
                        child: Text(widget
                            .itemSymbolController.wsKline.value.percentInterval
                            .toString()))

                    // Row(
                    //   children: [
                    //     Row(
                    //       children: [
                    //         Text('difference: '),
                    //         Container(
                    //             decoration: BoxDecoration(
                    //                 color: widget.itemSymbolController.wsKline.value.percentInterval == 0 ? Colors.grey : (widget.itemSymbolController.wsKline.value.percentInterval > 0 ? Colors.green : Colors.red)
                    //             ),
                    //             child: Text(widget.itemSymbolController.wsKline.value.percentInterval.toString()))
                    //       ],
                    //     ),
                    //     Row(
                    //       children: [
                    //         Text('time: '),
                    //         Text( SahaDateUtils().getHHMMSS(DateTime.fromMicrosecondsSinceEpoch(wsKline.kline.startTime)) )
                    //       ],
                    //     ),
                    //
                    //   ],
                    // ),
                  ],
                ),
              );
      }),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
