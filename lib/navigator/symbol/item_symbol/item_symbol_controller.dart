import 'dart:async';

import 'package:coinnews24h/binance_future/binance_future.dart';
import 'package:flutter/material.dart' hide Interval;
import 'package:get/get.dart';

import '../symbol_controller.dart';


class ItemSymbolController extends GetxController {
  SymbolController overviewController = Get.find();

  final String symbolInput;

  var lastClosePrice = .0.obs;
  var wsKline = ModelSymbolShow().obs;
  var lastTimeGet = DateTime.now();
  var lastOpen = null;

  late StreamSubscription<dynamic> klineStreamSub;

  ItemSymbolController(this.symbolInput) {
    startKlineStream();
  }

  void startKlineStream() {
    var stream = BINANCEFuture.klineStream(
      symbol: symbolInput,
      interval: Interval.INTERVAL_5m,
    );
    klineStreamSub = stream.listen(handleNewKline);
  }

  void handleNewKline(WsKlineEvent event) {
    lastClosePrice.value = event.kline.close;
    var difference = event.kline.close - event.kline.open;

    var percentInterval =
        ((difference / event.kline.open) * 100).toPrecision(2);

    var modelSymbolShow = ModelSymbolShow(wsKlineEvent: event)
      ..percentInterval = percentInterval;

    wsKline.value = modelSymbolShow;

    if (lastOpen != event.kline.open) {
      lastOpen = event.kline.open;
      lastTimeGet = DateTime.now();
      overviewController.setTimeLast(lastTimeGet);
    }

    modelSymbolShow.stepPrice = [event.kline.close];
    overviewController.addSymbolData(modelSymbolShow);
  }
}

