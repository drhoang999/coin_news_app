import 'dart:async';
import 'package:coinnews24h/binance_future/binance_future.dart';

import 'package:coinnews24h/const/const.dart';
import 'package:flutter/material.dart' hide Interval;
import 'package:get/get.dart';


const positiveThanSell = 'positiveThanSell';
// const totalSellThan30Percent = 'totalSellThan30Percent';
// const totalBuyThan30Percent = 'totalBuyThan30Percent';
// const totalZeroThan15Percent = 'totalZeroThan15Percent';
const timeThan55s = 'timeThan55s';
const totalDirectionThen80Percent = 'totalDirectionThen80Percent';
const differentLastTrend = 'differentLastTrend';

class ModelSymbolShow {
  WsKlineEvent? wsKlineEvent;
  double percentInterval = 0;
  List<double> stepPrice = [];

  ModelSymbolShow({this.wsKlineEvent});
}
BinanceFuture BINANCEFuture = BinanceFuture(
  key: API_KEY,
  secret: SECRET,
);
class SymbolController extends GetxController {
  var lastTrendIsSell = false.obs;
  //
  var listSymbol = <String>[].obs;
  var mapSymbol = Map<String, ModelSymbolShow>().obs;
  var lastTimeGet = DateTime.now().obs;
  var secondTime = 0.obs;
  var hasSend = false.obs;

  var mapCondition = {
    positiveThanSell: false,
    // totalSellThan30Percent: false,
    // totalBuyThan30Percent: false,
    // totalZeroThan15Percent: false,
    timeThan55s: false,
    totalDirectionThen80Percent: false,
    differentLastTrend: false,
  }.obs;
  var dataCalculate = DataCalculate(
      totalSymbol: 0,
      totalSell: 0,
      totalBuy: 0,
      totalZero: 0,
      allConditions: 1)
      .obs;


  SymbolController() {
    getTradablePairs();
  }

  late Timer _timer;


  DateTime? timeSetLastTrend;
  void changeLastTrend(bool isSell) {
    if (timeSetLastTrend == null ||
        (timeSetLastTrend!.difference(DateTime.now()).inSeconds).abs() < 30) {
      lastTrendIsSell.value = isSell;

      timeSetLastTrend = DateTime.now();
    }
  }


  void getTradablePairs() async {
    var response = await BINANCEFuture.exchangeInfo();
    if (response.isLeft) {
      //  tradablePairs = response.left;
    } else {
      var listSymbol2 = response.right.symbols.map((e) => e.symbol).toList();
      listSymbol(listSymbol2.sublist(0, 60));
      // for (var s in listSymbol) {
      //   listSymbol.add(s);
      //
      // }
    }
  }

  void setTimeLast(DateTime dateTime) {
    lastTimeGet.value = dateTime;
    secondTime.value = 0;
    hasSend.value = false;
  }

  double? getPriceCurrentBySymbol(String symbol) {
    if (mapSymbol[symbol] != null) {
      return mapSymbol[symbol]!.wsKlineEvent!.kline.close;
    }
  }

  void addSymbolData(ModelSymbolShow modelSymbolShow) {
    List<double> stepPrice = [];
    if (mapSymbol[modelSymbolShow.wsKlineEvent!.symbol] != null) {
      stepPrice
          .addAll(mapSymbol[modelSymbolShow.wsKlineEvent!.symbol]!.stepPrice);
      stepPrice.addAll(modelSymbolShow.stepPrice);
      modelSymbolShow.stepPrice = stepPrice;
    }

    mapSymbol[modelSymbolShow.wsKlineEvent!.symbol] = modelSymbolShow;



    if (mapSymbol[modelSymbolShow.wsKlineEvent!.symbol]!.stepPrice.length > 6) {
      mapSymbol[modelSymbolShow.wsKlineEvent!.symbol]!.stepPrice.removeAt(0);
    }

    var lengthBuy =
        mapSymbol.values.where((element) => element.percentInterval > 0).length;
    var lengthSell =
        mapSymbol.values.where((element) => element.percentInterval < 0).length;
    var lengthZero = mapSymbol.values
        .where((element) => element.percentInterval == 0)
        .length;

    var total30Percent = (mapSymbol.length * 30 * 0.01).floor();
    var total15Percent = (mapSymbol.length * 15 * 0.01).floor();
    var total80Percent = (mapSymbol.length * 80 * 0.01).floor();

    //allConditions
    //1 check Buy > Sell

    mapCondition[positiveThanSell] = lengthBuy > lengthSell;
    // mapCondition[totalSellThan30Percent] = lengthSell > total30Percent;
    // mapCondition[totalBuyThan30Percent] = lengthBuy > total30Percent;
    // mapCondition[totalZeroThan15Percent] = lengthZero > total15Percent;

    mapCondition[timeThan55s] = secondTime > 50;
    mapCondition[totalDirectionThen80Percent] = lengthBuy > lengthSell
        ? lengthBuy > total80Percent
        : lengthSell > total80Percent;

    var isSellTrend = lengthBuy < lengthSell;
    var lastTrendiSell = lastTrendIsSell.value;
    mapCondition[differentLastTrend] = lastTrendiSell != isSellTrend;

    dataCalculate(DataCalculate(
        totalSymbol: mapSymbol.length,
        totalSell: lengthSell,
        totalBuy: lengthBuy,
        totalZero: lengthZero,
        allConditions: 1,
        total15Percent: total15Percent,
        total30Percent: total30Percent,
        total80Percent: total80Percent));
  }


  bool validStepLong(List<double> listStep) {
    print(listStep);
    if (listStep.length < 5) {
      return false;
    }

    print(listStep);
    return
      listStep[4] >= listStep[3]
          && listStep[3] >= listStep[2]
          && listStep[2] >= listStep[1]
          && listStep[1] >= listStep[0];
  }

  bool validStepSell(List<double> listStep) {
    print(listStep);
    if (listStep.length < 5) {
      return false;
    }
    print(listStep);
    return
      listStep[4] <= listStep[3]
          && listStep[3] <=  listStep[2]
          && listStep[2] <=  listStep[1]
          && listStep[1] <=  listStep[0];
  }


}

class ActionData {
  String? direction;
  List<ModelSymbolShow>? listSymbol;
}

class DataCalculate {
  int? totalSymbol = 0;
  int? totalSell = 0;
  int? totalBuy = 0;
  int? totalZero = 0;
  int? allConditions = 0;
  int? total15Percent = 0;
  int? total30Percent = 0;
  int? total80Percent = 0;
  DataCalculate(
      {this.allConditions,
        this.totalZero,
        this.totalSymbol,
        this.totalSell,
        this.totalBuy,
        this.total15Percent,
        this.total30Percent,
        this.total80Percent});
}
