import 'package:coinnews24h/navigator/symbol/symbol_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'item_symbol/item_symbol.dart';

class SymbolScreen extends StatelessWidget {

  SymbolController symbolController = Get.put(SymbolController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Today's Cryptocurrency Prices"),),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            Obx(
                  () => Container(

                child:  Column(
                    children: symbolController.listSymbol
                        .map((element) {
                      return ItemSymbolWidget(
                          symbolInput: element.toString());
                    })
                        .cast<Widget>()
                        .toList(),
                  ),
                ),
            )

          ],
        ),
      ),
    );
  }
}