import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:coinnews24h/component/dialog/dialog.dart';
import 'package:coinnews24h/utils/user_info.dart';

import 'change_password/change_password.dart';

// ignore: must_be_immutable
class AccountScreen extends StatelessWidget {
  AccountScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Scaffold(
          appBar: AppBar(
            title: Text("Account"),
            actions: [
              IconButton(
                  icon: Icon(Icons.logout),
                  onPressed: () {
                    SahaDialogApp.showDialogYesNo(
                        mess: "Do you want to sign out?",
                        onOK: () {
                          UserInfo().logout();
                        });
                  })
            ],
          ),
          body: Container(
            child: ListView(
              children: [
                ListTile(
                  title: Text("Change the password"),
                  onTap: () {
                    Get.to(ChangePassword());
                  },
                )
              ],
            ),
          )),
    );
  }
}
