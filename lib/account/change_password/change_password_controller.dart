import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:coinnews24h/component/toast/saha_alert.dart';
import 'package:coinnews24h/data/repository/repository_manager.dart';

class ChangePasswordController extends GetxController {
  var resting = false.obs;
  var newPassInputting = false.obs;

  TextEditingController textEditingControllerNewPass =
      new TextEditingController();
  TextEditingController textEditingControllerOldPass =
      new TextEditingController();

  Future<void> onChange() async {
    resting.value = true;
    try {
      var loginData = (await RepositoryManager.loginRepository.changePassword(
        newPass: textEditingControllerNewPass.text,
        oldPass: textEditingControllerOldPass.text,
      ))!;

      SahaAlert.showSuccess(message: "Change password successfully");
      Get.back();
      resting.value = false;
    } catch (err) {
      SahaAlert.showError(message: err.toString());
      resting.value = false;
    }
    resting.value = false;
  }
}
