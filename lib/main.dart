import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:coinnews24h/saha_load_app.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'admob/admob_controller.dart';
import 'const/theme/theme_data.dart';
import 'data/remote/saha_service_manager.dart';
import 'data_app_controller.dart';
import 'load_data/load_firebase.dart';
import 'load_data/load_login.dart';
import 'load_data/serialise_and_navigate.dart';
import 'model/theme_model.dart';
import 'navigator/posts/view_post/view_post.dart';
import 'speak_controller.dart';

class GlobalVariable {
  /// This global key is used in material app for navigation through firebase notifications.
  /// [navState] usage can be found in [notification_notifier.dart] file.
  static final GlobalKey<NavigatorState> navState = GlobalKey<NavigatorState>();

  static int? postId;
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  SerialiseAndNavigate(message: message).navigate();
  await Firebase.initializeApp();
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  MobileAds.instance.initialize();
  LoadFirebase.initFirebase();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  SahaServiceManager.initialize();
  LoadLogin.load();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  DataAppController dataAppController = Get.put(DataAppController());
  SpeakController speakController = Get.put(SpeakController());
  AdmobController admobController = Get.put(AdmobController());
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: lightGreen,
      darkTheme: darkGreen,
      navigatorKey: GlobalVariable.navState,
      debugShowCheckedModeBanner: false,
      title: '歐易升級版',
      // theme: SahaUserPrimaryTheme,
      themeMode: ThemeMode.light,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      initialRoute: "homePage",
      home: SahaMainScreen(),
      routes: <String, WidgetBuilder>{
        "/view_post": (BuildContext context) => new ViewPostScreen(),
        "homePage": (context) => SahaMainScreen(),
        "secondScreen": (context) => ViewPostScreen(postId: 1500,),
      },
    );
  }
}
