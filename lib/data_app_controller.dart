import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'const/const.dart';
import 'const/theme/theme_data.dart';
import 'data/repository/repository_manager.dart';

class DataAppController extends GetxController {
  RxBool _isLightTheme = false.obs;

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  var hasLogin = false.obs;

  Future<String> getLinkConfig() async {
    try {
      var data = await RepositoryManager.configLinkRepository.getLink();
      for (var element in data!) {
        if (element.codeApp == NUM_CODE_APP) {
          return element.link!;
        }
      }
    } catch (err) {
      return "";
    }
    return "";
  }

  void onLogin() {
    hasLogin.value = true;
  }

  void logout() {
    hasLogin.value = false;
  }

  void onGet() {
    const time = const Duration(seconds: 300);
    Timer.periodic(time, (Timer t) => () {});
  }

  saveThemeStatus(int index) async {
    SharedPreferences pref = await _prefs;
    pref.setInt('theme', index);
  }

  getThemeStatus() async {
    var s = await _prefs;
    var index = s.getInt('theme') != null ? s.getInt('theme') : 0;

    Get.changeTheme(LIST_THEME_DATA[index!]);
  }

  void changeTheme(ThemeData themeData, int index) {
    Get.changeTheme(themeData);
    saveThemeStatus(index);
  }
}
