import 'package:get/get.dart';
import 'package:coinnews24h/const/const_database_shared_preferences.dart';
import 'package:coinnews24h/data/remote/response-request/auth/login_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../data_app_controller.dart';

class UserInfo {
  static final UserInfo _singleton = UserInfo._internal();


  String? _token;
  DataLogin? dataLogin;

  factory UserInfo() {
    return _singleton;
  }

  UserInfo._internal();


  Future<void> setData(DataLogin dataLogin) async {
    this.dataLogin = dataLogin;
  }

  Future<void> setToken(String? token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (token == null) {
      await prefs.remove(USER_TOKEN);
    } else {
      await prefs.setString(USER_TOKEN, token);
    }
    this._token = token;
  }

  String? getToken() {
    return _token;
  }

  Future<bool> hasLogged() async {
    await loadDataUserSaved();
    if (this._token != null && this._token!.length > 0)  {
      DataAppController v = Get.find();
      v.onLogin();
      return true;
    } else
      return false;
  }

  Future<void> loadDataUserSaved() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? tokenLocal = prefs.getString(USER_TOKEN) ?? null;
    this._token = tokenLocal;
  }

  Future<void> logout() async {
    //delete token
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(USER_TOKEN);

    DataAppController v = Get.find();
    v.logout();
    //delete message firebase
    //FirebaseMessaging.instance.deleteToken();

    //back screen
    //Get.offAll(() => LoginScreen());
  }
}
