import 'package:coinnews24h/data/remote/response-request/config_link/config_link.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:coinnews24h/const/env.dart';
import 'package:coinnews24h/data/remote/response-request/posts/all_posts.dart';
import 'package:coinnews24h/data/remote/response-request/success/success_response.dart';

import 'response-request/auth/check_exists_response.dart';
import 'response-request/auth/login_response.dart';
import 'response-request/auth/register_response.dart';
import 'response-request/config/config_response.dart';
import 'response-request/device_token/device_token_user_response.dart';
import 'response-request/image/upload_image_response.dart';
import 'response-request/posts/one_post_response.dart';
import 'response-request/profile/profile_response.dart';

part 'service.g.dart';

@RestApi(baseUrl: "$DOMAIN_API_CUSTOMER/api/")
abstract class SahaService {
  /// Retrofit factory
  factory SahaService(Dio dio) => _SahaService(dio);

  @GET("profile")
  Future<ProfileResponse> profile();

  @POST("favorite/{idPost}")
  Future<SuccessResponse> favorite(
      @Path("idPost") int idPost, @Body() Map<String, dynamic> body);

  //
  @POST("login")
  Future<LoginResponse> login(@Body() Map<String, dynamic> body);

  @POST("admin/reset_password")
  Future<SuccessResponse> resetPassword(@Body() Map<String, dynamic> body);

  @POST("admin/change_password")
  Future<SuccessResponse> changePassword(@Body() Map<String, dynamic> body);

  @POST("admin/login/check_exists")
  Future<ExistsLoginResponse> checkExists(@Body() Map<String, dynamic> body);

  /// image

  @POST("admin/images")
  Future<UploadImageResponse> uploadImage(@Body() Map<String, dynamic> body);

  /// Store

  @GET("settings_noti")
  Future<SettingNotiResponse> getDeviceTokenUser(
      @Query("device_token") String? deviceToken,);

  @POST("settings_noti")
  Future<SettingNotiResponse> updateDeviceTokenUser(
      @Body() Map<String, dynamic> body);

  //read post.dart
  @GET("posts")
  Future<PostsResponse> getAllPost(
      @Query("search") String? search,
      @Query("descending") bool? descending,
      @Query("sort_by") String? sortBy,
      @Query("page") int page,
      @Query("category") String? category);

  //read post.dart
  @GET("favorites")
  Future<PostsResponse> getAllPostFavorite(
    @Query("page") int page,
  );

  @GET("posts/{postId}")
  Future<OnePostResponse> getOnePost(
    @Path("postId") int postId,
  );

  @GET("posts/watched")
  Future<PostsResponse> getWatchedPost(
    @Query("page") int page,
  );

  //Register
  @POST("register")
  Future<RegisterResponse> register(@Body() Map<String, dynamic> body);

  //OTP
  @POST("send_otp")
  Future<SuccessResponse> sendOTP(@Body() Map<String, dynamic> body);

  //form_bonus
  @POST("admin/form_bonus")
  Future<SuccessResponse> sendFormBonus(@Body() Map<String, dynamic> body);



  @GET("config")
  Future<ConfigResponse> getConfig();

  @GET("config_link")
  Future<ConfigLinkResponse> getConfigLink();

}
