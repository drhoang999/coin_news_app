import 'package:coinnews24h/data/repository/handle_error.dart';

import '../../saha_service_manager.dart';
import 'config_response.dart';

class ConfigRepository {
  Future<DataConfig?> getConfig() async {
    try {
      var res = await SahaServiceManager().service!.getConfig();
      return res.data;
    } catch (err) {
      handleError(err);
    }
  }
}
