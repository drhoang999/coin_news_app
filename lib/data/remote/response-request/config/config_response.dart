class ConfigResponse {
  int? code;
  bool? success;
  String? msgCode;
  String? msg;
  DataConfig? data;

  ConfigResponse({this.code, this.success, this.msgCode, this.msg, this.data});

  ConfigResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    success = json['success'];
    msgCode = json['msg_code'];
    msg = json['msg'];
    data = json['data'] != null ? new DataConfig.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['success'] = this.success;
    data['msg_code'] = this.msgCode;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class DataConfig {
  int? id;
  String? successInputForm;
  String? createdAt;
  String? updatedAt;

  DataConfig({this.id, this.successInputForm, this.createdAt, this.updatedAt});

  DataConfig.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    successInputForm = json['success_input_form'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['success_input_form'] = this.successInputForm;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}