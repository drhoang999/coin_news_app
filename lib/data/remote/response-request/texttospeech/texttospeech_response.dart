class TextToSpeechResponse {
  String? audioContent;

  TextToSpeechResponse({this.audioContent});

  TextToSpeechResponse.fromJson(Map<String, dynamic> json) {
    audioContent = json['audioContent'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['audioContent'] = this.audioContent;
    return data;
  }
}