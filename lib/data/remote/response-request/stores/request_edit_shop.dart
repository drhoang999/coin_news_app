class RequestEditShop {
  RequestEditShop({
    this.name,
    this.address,
    this.logoUrl,
    this.hasUploadStore,
    this.linkGooglePlay,
    this.linkAppleStore,
    this.dateExpried,
  });

  String? name;
  String? address;
  String? logoUrl;
  bool? hasUploadStore;
  String? linkGooglePlay;
  String? linkAppleStore;
  DateTime? dateExpried;

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "address": address == null ? null : address,
        "logo_url": logoUrl == null ? null : logoUrl,
        "has_upload_store": hasUploadStore == null ? null : hasUploadStore,
        "link_google_play": linkGooglePlay == null ? null : linkGooglePlay,
        "link_apple_store": linkAppleStore == null ? null : linkAppleStore,
        "date_expried":
            dateExpried == null ? null : "${dateExpried!.toIso8601String()}",
      };
}
