import 'dart:convert';

import 'package:coinnews24h/model/banner.dart';

AddBannerResponse addBannerResponseFromJson(String str) =>
    AddBannerResponse.fromJson(json.decode(str));

String addBannerResponseToJson(AddBannerResponse data) =>
    json.encode(data.toJson());

class AddBannerResponse {
  AddBannerResponse({
    this.code,
    this.success,
    this.msgCode,
    this.msg,
    this.data,
  });

  int? code;
  bool? success;
  String? msgCode;
  String? msg;
  SahaBanner? data;

  factory AddBannerResponse.fromJson(Map<String, dynamic> json) =>
      AddBannerResponse(
        code: json["code"] == null ? null : json["code"],
        success: json["success"] == null ? null : json["success"],
        msgCode: json["msg_code"] == null ? null : json["msg_code"],
        msg: json["msg"] == null ? null : json["msg"],
        data: json["data"] == null ? null : SahaBanner.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "code": code == null ? null : code,
        "success": success == null ? null : success,
        "msg_code": msgCode == null ? null : msgCode,
        "msg": msg == null ? null : msg,
        "data": data == null ? null : data!.toJson(),
      };
}
