class SettingNotiResponse {
  int? code;
  bool? success;
  String? msgCode;
  String? msg;
  DataNoti? data;

  SettingNotiResponse(
      {this.code, this.success, this.msgCode, this.msg, this.data});

  SettingNotiResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    success = json['success'];
    msgCode = json['msg_code'];
    msg = json['msg'];
    data = json['data'] != null ? new DataNoti.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['success'] = this.success;
    data['msg_code'] = this.msgCode;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class DataNoti {
  int? id;
  int? customerId;
  String? deviceToken;
  String? deviceId;
  String? deviceType;
  bool? active;
  String? createdAt;
  String? updatedAt;

  DataNoti(
      {this.id,
        this.customerId,
        this.deviceToken,
        this.deviceId,
        this.deviceType,
        this.active,
        this.createdAt,
        this.updatedAt});

  DataNoti.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerId = json['customer_id'];
    deviceToken = json['device_token'];
    deviceId = json['device_id'];
    deviceType = json['device_type'];
    active = json['active'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['customer_id'] = this.customerId;
    data['device_token'] = this.deviceToken;
    data['device_id'] = this.deviceId;
    data['device_type'] = this.deviceType;
    data['active'] = this.active;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}