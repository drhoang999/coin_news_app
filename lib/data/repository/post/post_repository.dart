import 'package:coinnews24h/data/remote/response-request/posts/all_posts.dart';
import 'package:coinnews24h/data/remote/response-request/posts/one_post_response.dart';
import 'package:coinnews24h/data/remote/response-request/stores/request_edit_shop.dart';
import 'package:coinnews24h/data/remote/response-request/stores/response_edit_shop.dart';
import 'package:coinnews24h/data/remote/response-request/stores/stores_response.dart';
import 'package:coinnews24h/data/remote/saha_service_manager.dart';
import 'package:coinnews24h/data/repository/handle_error.dart';

class PostRepository {
  Future<OnePostResponse?> getOnePost(int idPost) async {
    try {
      var res = await SahaServiceManager()
          .service!
          .getOnePost(idPost);
      return res;
    } catch (err) {
      handleError(err);
    }
  }

  Future<PostsResponse?> getAllPost(
      String? search, bool? descending, String? sortBy, int page, String? category) async {
    try {
      var res = await SahaServiceManager()
          .service!
          .getAllPost(search, descending, sortBy, page, category);
      return res;
    } catch (err) {
      handleError(err);
    }
  }

  Future<PostsResponse?> getAllPostFavorite(
       int page) async {
    try {
      var res = await SahaServiceManager()
          .service!
          .getAllPostFavorite( page,);
      return res;
    } catch (err) {
      handleError(err);
    }
  }

  Future<PostsResponse?> getWatchedPost( int page,) async {
    try {
      var res = await SahaServiceManager()
          .service!
          .getWatchedPost(page);
      return res;
    } catch (err) {
      handleError(err);
    }
  }


}
