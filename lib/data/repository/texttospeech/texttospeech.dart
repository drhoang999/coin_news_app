import 'package:dio/dio.dart';
import 'package:coinnews24h/data/remote/response-request/texttospeech/texttospeech_response.dart';
import 'dart:convert';

import '../handle_error.dart';

class TextToSpeechRepository {
  Future<TextToSpeechResponse?> speech(String text, String gender) async {
    try {
      print("Start from google");

      var options = BaseOptions(
          receiveDataWhenStatusError: true,
          connectTimeout: 10 * 1000,
          receiveTimeout: 10 * 1000,
          responseType: ResponseType.json,
          headers: {
            "X-Goog-Api-Key": "AIzaSyC9sYpXXRuKvTgyVzqL1OqVvr2EOwb07go",
            "content-type": "application/json"
          });

      var dio = Dio(options);

      final response = await dio.post(
          'https://texttospeech.googleapis.com/v1/text:synthesize',
          data: {
            "input": {
              "text": text
            },
            "voice": {
              "languageCode": "vi-VN",
              "name":gender == "FEMALE" ? "vi-VN-Wavenet-A" : "vi-VN-Wavenet-D",
              "ssmlGender": gender
            },
            "audioConfig": {"audioEncoding": "MP3"}
          });

      return TextToSpeechResponse.fromJson(response.data);
    } catch (err) {
      print(err.toString());

      handleError("Lỗi xử lý giọng nói");
    }
  }
}
