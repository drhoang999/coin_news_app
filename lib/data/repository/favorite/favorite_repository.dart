import 'package:coinnews24h/data/remote/response-request/auth/register_response.dart';
import 'package:coinnews24h/data/remote/saha_service_manager.dart';

import '../handle_error.dart';

class FavoriteRepository {
  Future<bool?> favorite({int? idPost, bool? isFavorite}) async {
    try {
      var res = await SahaServiceManager().service!.favorite(idPost!, {
        "is_favorite": isFavorite,
      });
      return res.success;
    } catch (err) {
      handleError(err);
    }
  }
}
