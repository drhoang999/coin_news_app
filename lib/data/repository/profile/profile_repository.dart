import 'package:coinnews24h/data/remote/response-request/profile/profile_response.dart';
import 'package:coinnews24h/data/remote/saha_service_manager.dart';
import 'package:coinnews24h/data/repository/handle_error.dart';

class ProfileRepository {
  Future<DataProfile?> getProfile() async {
    try {
      var res = await SahaServiceManager().service!.profile();
      return res.data;
    } catch (err) {
      handleError(err);
    }
  }
}
