import 'package:coinnews24h/data/remote/response-request/auth/register_response.dart';
import 'package:coinnews24h/data/remote/saha_service_manager.dart';

import '../handle_error.dart';

class OtpRepository {
  Future<bool?> sendOTP({String? phoneNumber}) async {
    try {
      var res = await SahaServiceManager().service!.sendOTP({
        "phone_number": phoneNumber,
      });
      return res.success;
    } catch (err) {
      handleError(err);
    }
  }
}
