import 'package:coinnews24h/data/remote/response-request/auth/register_response.dart';
import 'package:coinnews24h/data/remote/saha_service_manager.dart';

import '../handle_error.dart';

class FormBonusRepository {
  Future<bool?> sendFormBonus(
      {String? phone, String? email, String? address, String? otp}) async {
    try {
      var res = await SahaServiceManager().service!.sendFormBonus({
        "email": email,
        "phone_number": phone,
        "address_wallet": address,
        "otp": otp
      });
      return res.success;
    } catch (err) {
      handleError(err);
    }
  }
}
